﻿
class BubbleSort
{

    public int[,] Sort(int[,] array)
    {

        int[,] numbers = new int[10, 2];

        bool sorting = true;

        while (sorting)
        {
            int counter = 0;
            for (int i = 0; i < array.GetLength(0) - 1; i++)
            {
                if (array[i, 0] > array[i + 1, 0])
                {
                    var tempX = array[i, 0];
                    var tempY = array[i, 1];

                    array[i, 0] = array[i + 1, 0];
                    array[i, 1] = array[i + 1, 1];
                    array[i + 1, 0] = tempX;
                    numbers[i + 1, 1] = tempY;
                    counter++;
                }
            }
            if (counter == 0)
            {
                sorting = false;
            }
        }
        return array;
    }
}