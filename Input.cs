﻿class Input
{
    int[,] numbers = new int[10, 2];

    public void GetInput()
    {
        for (int i = 0; i < 10; i++)
        {
            int x = 0;
            int y = 0;
            Console.WriteLine("Input the X value for index [" + i + "] : ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Input the Y value for index  [" + i + "] : ");
            y = int.Parse(Console.ReadLine());
            numbers[i, 0] = x;
            numbers[i, 1] = y;
        }
        Console.WriteLine("Parsing...");
    }

    public int[,] GetArray()
    {
        return numbers;
    }
}