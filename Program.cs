﻿
Input input = new Input();
BubbleSort sort = new BubbleSort();

input.GetInput();
int[,] values = sort.Sort(input.GetArray());

for (int i = 0; i < values.GetLength(0); i++)
{
    Console.WriteLine(values[i, 0] + "," + values[i, 1]);
}
